/**
 * 编辑文章
 * Created by mlr on 2017/3/14.
 */
var path = require('path'),
    fs  = require('fs'),
    config = require('config'),
    util = require(path.join(APP_BASE.S, 'util'));

exports = module.exports = function(req, res) {
    var _data = req.query;
    var docId = _data['docId'];
    var docData = util.findById(docId);
    if (docData) {
        var md_folder = config.get('MD_FILE_FOLDER');
        var md_path = md_folder + docData['_id'] + '.md';
        var md_content = fs.readFileSync(md_path);
        docData['mdContent'] = md_content;
    }
    res.render('index', { title: '编辑文档', data: docData, moduleName: 'doc/edit' });
};



