/**
 *  查询列表
 * Created by mlr on 2016/9/6.
 */
var path = require('path')
    ,   util = require(path.join(APP_BASE.S, 'util'));

exports = module.exports = function(req, res) {
    var result = util.getData(true);
    //res.send({ err: 0, data: result});
    res.send(result.list);
};
