/**
 * 保存所编辑的文件
 * // -- TODO 连接mongoDB, 保存到mongoDB数据库中
 * Created by mlr on 2017/3/10.
 */
var path = require('path'),
    fs  = require('fs'),
    config = require('config'),
    util = require(path.join(APP_BASE.S, 'util'));

exports = module.exports = function(req, res) {
    var _data = req.query;
    var docId = _data['docId'], title = '';
    var docData = util.findById(docId);
    if (docData) {
        title = docData['fileName'];
    }
    res.render('doc/preview', { err: 0, docId: docId, title: title});
};


