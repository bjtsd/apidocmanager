# 知识点资源平台对外接口文档

**当前版本**：V0

**更新说明**：
    1. 初始化接口文档!
	
## 目录

[TOC]

## 一、基础约定
- **约定**：

    ①　接口状态：字体颜色为黑色代表启用;字体颜色为灰色代表停用；

    ②　是否必填：Y：是,N: 否

    ④  接口只支持GET方式请求

- **访问地址**:

    http://{host}/outside?eventType={eventType}&entity={entity}&v={v}&lang={lang}&mediaSource={mediaSource}

    ①　host：接口访问地址:
    >测试环境地址:test.teacherfamily.net:8027

    ② eventType和entity：见各接口具体说明

    ③ v ： 接口版本，见文档最开头,当前应填写V0
	
    ④ mediaSource ：设备来源；IOS：0；android：1

    ⑤ appCode ：(此为固定值，对应云用户平台的appCode)


## 二、接口说明

- ### 1、获取知识点列表

#### 输入参数

<table> <tr> <th>eventType</th> <th colspan="5" align="left">out.knowledge.page</th> </tr> <tr> <th rowspan="6">输入参数</th> <th style="background-color: rgba(24, 24, 70, 0.3);">参数</th> <th style="background-color: rgba(24, 24, 70, 0.3);">参数说明</th> <th style="background-color: rgba(24, 24, 70, 0.3);">数据类型</th> <th style="background-color: rgba(24, 24, 70, 0.3);">是否必填</th> <th style="background-color: rgba(24, 24, 70, 0.3);">备注</th> </tr> <tr> <td>group</td> <td>标签名</td> <td>varchar(64)</td> <td>是</td> <td>见字典</td></tr><tr> <td>from</td> <td>依赖的id</td> <td>varchar(64)</td> <td>是</td> <td>-</td></tr><tr> <td>start</td> <td>第几页</td> <td>int</td> <td>否</td> <td>默认为0</td> </tr> <tr> <td>limit</td> <td>每一页显示多少条数据</td> <td>int</td> <td>否</td> <td>默认20</td> </tr> </table>

输入参数示例:

``` json
{
	"group":"content",
	"from": "58e92a1cb062efd688bc2445"
}
```

#### 输出参数

<table> <tr> <th rowspan="12">输出参数</th> <th colspan="5" style="background-color: rgba(24, 24, 70, 0.3);">参数</th> <th style="background-color: rgba(24, 24, 70, 0.3);">参数说明</th> <th style="background-color: rgba(24, 24, 70, 0.3);">数据类型</th> <th style="background-color: rgba(24, 24, 70, 0.3);" colspan="7">备注</th> </tr> <tr> <td colspan="5">err</td> <td>是否成功</td> <td>int</td> <td>0:成功;1:其他</td> </tr> <tr> <td colspan="5">errMsg</td> <td>错误信息</td> <td>varcahr(64)</td> <td>仅在err!=0时有值</td> </tr> <tr> <td rowspan="9">data</td> <td colspan="5" align="right">数据集</td> <td>Object</td> <td>-</td> </tr> <tr> <td colspan="4">count</td> <td>结果统计</td> <td>int</td> <td>-</td> </tr> <tr> <td colspan="4">list</td> <td>数据列表</td> <td>JSONArray</td> <td>-</td> </tr> <tr> <td rowspan="6"></td> <td colspan="3">id</td> <td>类别主键</td> <td>varcahr(64)</td> <td>-</td> </tr> <tr> <td colspan="3">label</td> <td>内容</td> <td>varcahr(64)</td> <td>-</td> </tr> </table>

正确输出参数示例:

```json
    {
        "err": 0,
        "data": {
            "count": 2,
            "list": [{
                "id":"58e4d66010f56f29d8dc3e93",
				"label": "数与式"
            },{
                "id": "58e4d66010f56f29d8dc3e99",
				"label": "整式"
            }]
        }
    }
```

错误输出参数示例：

```json
{
    "err": 1,
    "errMsg": "系统操作异常"
}
```

#### 调用示例

注：请将下述的地址用浏览器打开。

[1.  返回错误信息](http://test.teacherfamily.net:8027/outside?eventType=out.knowledge.page&v=V0&mediaSource=3&appCode=51cp&entity={"group":"content","from":"58e92a1cb062efd688bc2445"} "1.  返回错误信息")

[2. 获取知识点第一级](http://test.teacherfamily.net:8027/outside?eventType=out.knowledge.page&v=V0&mediaSource=3&appCode=557b926f756a352f51c47837&entity={"group":"knoNames"} "2. 获取知识点第一级")

[3. 获取初中数学下的所有章](http://test.teacherfamily.net:8027/outside?eventType=out.knowledge.page&v=V0&mediaSource=3&appCode=557b926f756a352f51c47837&entity={"group":"charpters","from":"58e92a1cb062efd688bc2445"} "3. 获取初中数学下的所有章")

[4. 获取初中数学下的所有小节](http://test.teacherfamily.net:8027/outside?eventType=out.knowledge.page&v=V0&mediaSource=3&appCode=557b926f756a352f51c47837&entity={"group":"parts","from":"58e92a1cb062efd688bc2445"} "4. 获取初中数学下的所有小节")

[5. 获取初中数学下的所有知识点](http://test.teacherfamily.net:8027/outside?eventType=out.knowledge.page&v=V0&mediaSource=3&appCode=557b926f756a352f51c47837&entity={"group":"content","from":"58e92a1cb062efd688bc2445"} "5. 获取初中数学下的所有知识点")

[6. 获取初中数学章为数与式下的所有小节](http://test.teacherfamily.net:8027/outside?eventType=out.knowledge.page&v=V0&mediaSource=3&appCode=557b926f756a352f51c47837&entity={"group":"parts","from":"58e92a1db062efd688bc2446"} "6. 获取初中数学章为数与式下的所有小节")

## 四、字典数据

<table><tr style="background-color: rgba(24, 24, 70, 0.3);"><th>modelName</th><th>ItemKey</th><th>说明</th></tr><tr><td rowspan="4">Group</td><td>knoNames</td><td>书本，如初中数学</td></tr><tr><td>charpters</td><td>章</td></tr><tr><td>parts</td><td>节</td></tr><tr><td>content</td><td>知识点内容</td></tr><tr><td rowspan="4">mediaSource</td><td>0</td><td>ios</td></tr><tr><td>1</td><td>ios</td></tr><tr><td>2</td><td>web端</td></tr><tr><td>3</td><td>其他</td></tr><tr><td rowspan="4">appCode</td><td>557b926f756a352f51c47837</td><td>51测评服务端</td></tr></table>

## 五、错误码

<table><tr style="background-color: rgba(24, 24, 70, 0.3);"><th>错误码</th><th>中文信息</th></tr><tr><td>1</td><td>系统操作异常</td></tr><tr><td>2</td><td>请求数据格式有误,请根据接口文档核对</td></tr><tr><td>3</td><td>eventType不存在</td></tr><tr><td>4</td><td>entity不是标准json格式</td></tr><tr><td>5</td><td>语言信息输入有误</td></tr><tr><td>6</td><td>appCode错误,请验证是否有权限访问数据</td></tr></table>