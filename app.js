var express = require('express')
    ,   path    = require('path')
    ,   fs      = require('fs')
    ,   logger  = require('morgan')
    ,   favicon = require('serve-favicon')
    ,   timeout      = require('connect-timeout')
    ,   bodyParser   = require('body-parser')
    ,   cookieParser = require('cookie-parser')
    ,   config       = require('config').get('SYS')
    ,   webConfig    = require('config').get('web')
    ,   debug = process.env.NODE_ENV !== 'production'
    ,   appConfig = require('config');

var app = express();
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.set('TITLE', webConfig.title);
app.set('HOST', config.host);
app.set('UUID', config.uuid);
app.set('SECRET', config.secret);
app.set('WEBCONFIG', webConfig);
app.set('SYS_UPLOAD_TMP_DIR', config.upload.tempdirName);
app.set('SYS_UPLOAD_DIR', config.upload.dirName);
app.set('SYS_FILE_UP_NAME',   config.upload.upName); //设置文件上传的通用名

app.set('SYS_DATA_LIST_DIR',   require('config').get('DATA_PATH')); //
app.set('SYS_MD_FOLDER',   appConfig.get('MD_FILE_FOLDER')); //设置文件上传的通用名
app.set('SYS_MD_HTML_FOLDER',   appConfig.get('MD_HTML_FILE_FOLDER')); //设置文件上传的通用名

app.use(timeout('60s', {respond: false}));
app.use(favicon(__dirname + '/public/favicon.ico'));
debug && app.use(logger('dev'));
app.use(bodyParser.json({limit: '100mb'}));
app.use(bodyParser.urlencoded({ limit: '100mb', extended: false }));
app.use(cookieParser(app.get('SECRET')));
app.use(express.static(path.join(__dirname, 'public')));
fs.exists(app.get('SYS_UPLOAD_TMP_DIR'), function(exists) {
  if(!exists) { fs.mkdirSync(app.get('SYS_UPLOAD_TMP_DIR')); }
});
fs.exists(app.get('SYS_UPLOAD_DIR'), function(exists) {
    if(!exists) { fs.mkdirSync(app.get('SYS_UPLOAD_DIR')); }
});
fs.exists(app.get('SYS_MD_FOLDER'), function(exists) {
    if(!exists) { fs.mkdirSync(app.get('SYS_MD_FOLDER')); }
});
fs.exists(app.get('SYS_MD_HTML_FOLDER'), function(exists) {
    if(!exists) { fs.mkdirSync(app.get('SYS_MD_HTML_FOLDER')); }
});
fs.exists(app.get('SYS_DATA_LIST_DIR'), function(exists) {
  if(!exists) {
      if (!fs.existsSync(path.join(__dirname, 'routes/data'))) {
          fs.mkdirSync(path.join(__dirname, 'routes/data'));
      }
      fs.appendFileSync(app.get('SYS_DATA_LIST_DIR'), String(JSON.stringify({count: 0, list:[]})));
  }
});
fs.exists(path.join(__dirname, 'html'), function(exists) {
  if(!exists) { fs.mkdirSync(path.join(__dirname, 'html')); }
});
fs.exists(path.join(__dirname, 'config'), function(exists) {
  if(!exists) { fs.mkdirSync(path.join(__dirname, 'config')); }
});
app.use(express.static('./html', {maxAge: 31557600000}));

global.APP_BASE = {
    ROOT: path.join(__dirname),
    R: path.join(__dirname, 'routes'),
    A: path.join(__dirname, 'routes/action'),
    T: path.join(__dirname, 'routes/API'),
    F: path.join(__dirname, 'routes/file'),
    H: path.join(__dirname, 'routes/html'),
    S: path.join(__dirname, 'routes/service')
};

var router = require('./routes/router');
app.set('SYS_APP__PATH', __dirname);
app.use(router(app));

module.exports = app;
