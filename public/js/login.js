/**
 * 用户登录
 * TODO 连接mongoDB验证用户角色
 * TODO 添加数据验证
 * Created by mlr on 2017/3/9.
 */
$(function() {
    var loginName = $('input[name = "loginName"]').val();
    var password  = $('input[name = "password"]').val();
    var $tipMsg = $('#tip-errMsg');
    if (!loginName || !password) {
        $tipMsg.html('您输入的用户名或密码不能为空，请重新输入！');
    } else if (loginName != 'admin' && password != 'admin') {
        $tipMsg.html('您输入的用户名或密码有误，请重新输入！');
    } else {
        window.location.href = '/doc/list';
    }
});
