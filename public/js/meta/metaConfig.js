/**
 * 接口文档 模型说明
 * Created by mlr on 2017/3/14.
 */
META_MODELS = {
    Docs: {
        list: {
          attrs: [ '_id', 'fileName', 'updateTime']
        },
        attrs: {
            _id: {field: '_id', title: '_id', width: '30%' },
            fileName: {field: 'fileName', title: '文件名', width:'40%', sortable: true,
                formatter: function (value, row) {
                    var url = '/doc/preview.html?docId=' + row['_id'];
                    return ['<a href="', url ,'" target="_blank">', value ,'</a>'].join('');
                }
            },
            createTime: { field: 'createTime', title: '创建时间', sortable: true, width:'30%' },
            updateTime: { field: 'updateTime', title: '更新时间', sortable: true, width:'15%' }
        }
    }
};

