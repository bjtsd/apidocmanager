/**
 * Created by mlr on 2017/3/14.
 */
$(function() {
    var $tableList = $('#table-list');
    var getColumns = function() {
        var columns = [], items = META_MODELS['Docs']['attrs'];
        var showTableAttrs = META_MODELS['Docs']['list']['attrs'];
        showTableAttrs.forEach(function(v){
            columns.push(items[v]);
        });
        // 添加操作事件
        var _event = {
            title: '操作', width: '20%',
            formatter: function(v, row) {
                return [
                    '<a class="btn btn-default btn-sm opt-mod">编辑</a>'
                    //,'<a class="btn btn-danger btn-sm opt-dtl" style="margin-left:10px;">删除</a>'
                ].join('');
            },
            events: {
                'click .opt-mod': function(e, v, row, idx){
                    // -- 开启模态框. TODO 输入编辑密码, 不知道的不能进行编辑功能.
                    window.location.href = '/doc/edit?docId=' + row['_id'];
                },
                'click .opt-dtl': function(e, v, row, idx){
                    if (confirm('确定删除?')) {
                        /*commonUtil.httpPost('/submit.do', { _id: row._id }, function(rsp) {
                            if (parseInt(rsp.err) == 0) {
                                $tableList.bootstrapTable('remove', {
                                    field: '_id',
                                    values: [row._id]
                                });
                            }
                        });*/
                    }
                }
            }
        };
        columns.push(_event);
        return columns;
    };
    $tableList.bootstrapTable({
        columns: getColumns(),
        detailFormatter: function(index, row) {
            var html = [], tips = META_MODELS['Docs']['attrs'];
            $.each(row, function (key, value) {
                if (tips[key]) {
                    html.push('<p><b>' + tips[key].title + ':</b> ' + value + '</p>');
                }
            });
            return html.join('');
        }
    });
});
