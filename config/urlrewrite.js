var path = require('path');
module.exports = {
    'apk.update.check': path.join(__dirname, '../routes/API/check'),
    'file.download': path.join(__dirname, '../routes/file/download')
};