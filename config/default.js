/**
 * default config
 * Created by mlr on 2016/8/1.
 */
var path = require('path');
var title = "接口文档管理系统";
module.exports = {
    DATA_PATH: path.join(__dirname, '../routes/data/data.json'),
    MD_FILE_FOLDER: path.join(__dirname, '../routes/data/md/'),
    MD_HTML_FILE_FOLDER: path.join(__dirname, '../views/doc/html/'),
    SYS : {
        host: 'http://localhost:3000/',
        webport: 3000,
        //urlrewrite: require('./urlrewrite.js'),
        upload: {
            tempdirName: path.join(__dirname, '..', 'tmp'),
            dirName: path.join(__dirname, '..', 'upload'),
            upName : 'file',
            limit  : 5*1024*1024
        },
        errMsg: {
            base: {
                'DB_EXPETION': { err: 1, errMsg: '操作异常!' },
                'POST_EMPTY': { err: 2, errMsg: '提交数据为空!' },
                'VERSION_ERROR': {err: 3, errMsg: '版本格式错误'}
            }
        },
        succMsg: {
            'IS_LAST': {err: 4, errMsg: '当前版本已是最新!'}
        }
    },
    web: {
        title: title
    }
};
